//Get the button
var mybutton = document.getElementById("myBtn");

// When the user scrolls down 20px from the top of the document, show the button
window.onscroll = function() {scrollFunction()};

function scrollFunction() {
  if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
    mybutton.style.display = "block";
  } else {
    mybutton.style.display = "none";
  }
}

// When the user clicks on the button, scroll to the top of the document
function topFunction() {
  document.body.scrollTop = 0;
  document.documentElement.scrollTop = 0;
}
$(window).scroll(function(){
  var sticky = $('.menu-sumary'),
      scroll = $(window).scrollTop();

  if (scroll >= 300) sticky.addClass('fixed');
  else sticky.removeClass('fixed');
});

//btnn menu
$('.ti-menu').click(function(){
  $('.mobile-nav').addClass("show-menu-mobile");
  $('.icon-menu-mb').addClass("show-icon-mobile");
  $('.bg-menu').addClass("show-bg-mobile");
});
$('.ti-close').click(function(){
  $('.mobile-nav').removeClass("show-menu-mobile");
  $('.icon-menu-mb').removeClass("show-icon-mobile");
  $('.bg-menu').removeClass("show-bg-mobile");
});
//btn reply
$('.icon-reply').click(function(){
  $('.box-reply').addClass("show-box-reply");
});